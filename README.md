# BIOGRAFÍA 
___

## Acerca de Miguel 

![Miguel](https://gitlab.com/MigueReina101096/ejercicio1/blob/rama-guest/Fotografias/miguel.jpg) 

**Miguel Reina** del norte de la ciudad de Quito. Su más preciado sueño llegar a ser el mejor en el área en el que me desenvuelva tanto como persona, hijo, hermano y profesional. Estudiante de ingeniería en sistemas en la Escuela Politécnica Nacional. Interesado por el aprendizaje diario e interesado por aquellas cosas que ocurren magicamente, ya que antes de existir, eran cosas inalcanzables. Todo ello hace de Miguel una persona amigable, carismatica y único en todo.

***Como me pueden encontrar***


Correo electronico: miguel.reina@epn.edu.ec

Número de celular: 0998535916

Correo electronico: miguel_rg_esteban@hotmail.com

Facebook: miguel_rg_esteban@hotmail.com

[**¡Más acerca de mi!**](https://gitlab.com/MigueReina101096/ejercicio1/blob/master/informacion/README.md)




Para la revisión de la carpeta "Reporte", dirigirse a la rama "rama-guest".