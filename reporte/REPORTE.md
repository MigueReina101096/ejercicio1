# REPORTE


## Como Guest


#### Aspectos Positivos


* Se puede clonar el proyecto. 
* Se puede crear una rama localmente.


#### Aspectos Negativos


* No se puede crear una rama como Guest.
* No permite modificar nada.


## Como Reporter


#### Aspectos Positivos


* Se puede crear una rama localmente.
* Se puede crear y cerrar issues con labels, fechas y asignaciones para la mejora del proyecto.


#### Aspectos Negativos


* No se puede crear una rama como Reporter.
* No permite modificar nada.


## Como Developer


#### Aspectos Positivos


* Si se puede crear una rama como Developer.
* Hacer push y modificaciones.
* Sirven los comandos:


1. git status
2. git branch 
3. git add .
4. git push -u origin nombre.rama
5. git branch -d nombre.rama
6. git fetch


#### Aspectos Negativos


* Con permisos de developer no se puede hacer Merge Request entre ramas secundarias con la rama master.


## Como Maintainer


#### Aspectos Positivos


* Si se puede crear una rama como Maintainer.
* Si se puede eliminar ramas.
* Si se puede crear archivos.
* Sirven los comandos:


1. git status
2. git branch 
3. git add .
4. git push -u origin nombre.rama
5. git branch -d nombre.rama
6. git fetch


#### Aspectos Negativos


* Con permisos de maintainer se puede hacer Merge Request entre cualquier tipo de rama en el proyecto a realizar.